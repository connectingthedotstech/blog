Title: Django/DRF development stack
Date: 2019-02-24
Modified: 2019-02-24
Category: Django,
Tags: django, drf, development, python
Slug: django-development-stack
Authors: me
Summary: What I use to develop app in django/DRF and how I got there.



I have couple of years experience in developing backend applications using [Django](https://www.djangoproject.com/) and especially [Django REST framework](https://www.django-rest-framework.org/), which are awesome frameworks. In this post I would like to share my setup I use in daily work and its history.

I work on few projects at a time, so good separation is a must,  also projects on which I'm working on practically always need some extra stuff like SQL database (postgres), celery which include external broker (RabbitMQ or Redis) and more.

I've tried few things from using system python interpreter and global space for packages, through virtualenvs, dedicated vagrant VMs and recently starting to adopt more and more of docker for local development. Let's check how they worked out:

## History

#### System interpreter
First thing when I started my adventure in python, but very quickly my peers suggested to use at least virtualenvs for my own future self sake.

#### [virtualenv](https://virtualenv.pypa.io/en/latest/) & [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)
Next step was separating python environments per project using virtualenv. It works really well, specially with virtualenvwrapper which improves usability of virtualenv. Similar solution for that would be [pyenv](https://github.com/pyenv/pyenv) which also integrate with virtualenv by [pyenv-virtualenvwrapper](https://github.com/pyenv/pyenv-virtualenvwrapper). If you wonder what are the differences I found this [stackoverflow post](https://stackoverflow.com/questions/29950300/what-is-the-relationship-between-virtualenv-and-pyenv) very good at explaining things.

#### [Vagrant](https://www.vagrantup.com/)
As I have few ongoing projects at the same time there was need to not only separate python environment but also other things. Different projects used different versions of Postgres, Redis, Elasticsearch and so on. At that time Docker was quite a novelty, deadlines were approaching, so decision was made to use bare VMs and Vagrant as tool to manage them and share configuration among team. This works well, but of course they were hiccups. Mainly every team member has slightly different local configuration. We handled that with ENV variables, some bash scripts and carry on. Also big disadvantage was fully fledge VM to maintain, but there was a solutions for that also...

#### [Docker](https://www.docker.com/) with [docker compose](https://docs.docker.com/compose/)
Eventually times has come to join hype train of containerization. My peers mostly went fully on it using docker for every component of their projects. I used docker only for external dependencies (db, redis, frontend, etc) and keeping python part of project on host machine. My main reason was for debugging. I had this thing, that I like to debug using `ipdb` mainly to get familiar with console. Trying to debug running application inside container is doable and as time go, tools are better and better, but for now it is more familiar for me.

### Present day
How it looks for time of writing this post?
- Docker and docker compose for handling external dependencies
- Pyenv with pip-tools for handling python on host machine

## Packages
Of course going with classical [XKCD comic](https://xkcd.com/353/) power of python comes for its wast ecosystem of libraries, not only for Django and DRF, but now let's focus on them. Below are list of packages that helps me a lot in my everyday work:

#### [DRF extensions](https://github.com/chibisov/drf-extensions)

Set of useful addictions for DRF that didn't make into main framework code. My favorite feature are nested routers and nested viewsets.

#### [django ordered model](https://github.com/bfirsh/django-ordered-model)
Really nice package when you want to have custom order used defined models ordering in your system. Nice, simple api and admin panel with drag & drop. Help me quite a few times.

#### [Django Scheduler](https://github.com/llazzaro/django-scheduler)
If you have a requirement to implement some kind of scheduler/planner with recurring events (for example training plan) and link it with calendar this is go to for you. Really nice handles all time and dates arithmetic and provides nice integration with django. In my case I had to customize it a little bit, but source code is organized well and readable, so learning this library from source is IMO better than from documentation, which is quite basic.

#### [drf-yasg](https://github.com/axnsan12/drf-yasg)
Newest in bunch. One of the biggest pain in all software development is creating and maintaining documentation. Documentation for REST API is even more troublesome as you have to include not only description how it work, but also urls, request/response schemas, status codes, etc. Swagger is most popular tool for that, but who wants to write this `swagger.json` by hand? Not me, so I googled a little and found this. For now works really well for standard DRF stuff, but of course if you have something custom in your flow, you will have to write some code.

#### [pytest](https://docs.pytest.org/en/latest/)
From the beginning I've used testing facility included in django which is standard python library `unittest`. In my latest project I started to utilize pytest and after some newbie fails I'm getting to speed with it and starting to see abilities of this library. Definitely I plan to know it more and use it more efficiently.

## Editors
My current favorite editor is [VSCode](https://code.visualstudio.com/). It's nice, fast, quick to learn and python support is better and better. In my company although I got [pyCharm](https://www.jetbrains.com/pycharm/) (Professional Edition) and also starting to explore this IDE. Why I didn't use pyCharm in the first place? I came from .NET world and there is only one king, Visual Studio. This is truly Integrated Development Environment in which you can do almost everything. Switching to python and linux, I wanted to learn more using terminal. Sitting all the time in IDE and using mouse to control it wouldn't help.


That's all for now, hope you found this post somewhat helpful.

Cheers!