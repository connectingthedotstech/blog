Title: What is software development process (for me)
Date: 2019-12-10
Modified: 2019-12-10
Category: Software Craftsmanship,
Tags: development, general thoughts,
Slug: software-development-process
Authors: me
Summary: What is software development process for me

<!-- Building software is all about abstractions and modeling reality. It's hard to build something right if you have no understanding how is this working.
This is one of those hard parts in everyday software industry that is not related to code or technical stuff directly.

Another one would be handling non-technical customers with whole software development process cycle. Even if as the time of this writing (2019) we have all this amazing tools/frameworks to help us deliver cool stuff,
it's still hard and time consuming. One of the reasons, at least for me, it's about modeling reality and view of non-technical person about software development.
From this angle I have two main types of work:
 - That is easy, quick and amazing from customer perspective (change colors on UI)
 - That is nontrivial, time consuming and totally unseen by customer (integration with payment system, generate complicated report, some complex process on backend)

If you are working on smaller projects, for bigger this is also applies, but in bigger projects, there are more people and if you are just line developer it can be missed. -->

What is software development process?
Well, you'll find in internet many definitions and explanations from people far smarter than me.

[You can start your research on wikipedia.](https://en.wikipedia.org/wiki/Software_development_process)


So this is more like my personal view based on my experience, observations and thoughts. I'm not trying to prove of disprove anything. Just to share.

tl;dr
## *Software development process is like shipping stuff across the ocean.*

🎉🎉🎉

Now, let's dissect this.

Why across the ocean?

- it's take time, conditions can change and on the middle of the ocean you are on your own

Why shipping?

- this is well recognized and standardized human activity, but still it can be tricky, especially with non standard cargo (which your customers startup probably is)
- you need target/goal/destination to make this activity sense.

    > *Even the biggest ship with best crew is useless without destination port*

- you need to know what and where you are shipping, to produce how and when it will arrive

<!-- With that software development is similar to this because:
- Young but working hard to create set of good practices, procedures to make life easier and better end product -->


### **YOU NEED TO KNOW YOUR GOAL BEFORE YOU LEAVE**

For me this is most important thing, especially for greenfield startups. Sounds obvious right? But you would be surprised how often is missed.
And it could be devastating for the whole endeavor.

You need to have compass set and your goal marked on map before everything.
It must be your customer goal for this product/project. S/he has to define it, write it down and keep in front of her/his eyes all the time.
<!-- This is should set course for all the work. -->

Why? To prioritize things and make decision making easier.

How often do you have some decision to make about how do things or what tool to use and you are struggling?
Now put this choice in context of your goal. Is this put me closer to our goal? Is time spend for learning this new thing is worth it? Maybe You Aren't Gonna Need It ([YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it))?
Maybe some simple [PoC](https://en.wikipedia.org/wiki/Proof_of_concept) will be enough for now, just to validate things if we are on right course to our destination port. If yes, than good, carry on, build proper thing, if not, that's fine too, it didn't cost to much time and at least we know that it is not right solution, carry on.

Another thing that is quite common in our daily work is need to explain your decision to customer.
With proper goal set, it's easier, because everyone wants the same. You speak the same language.
For me this is especially good for feature creep.

<!-- Knowing common goal is easier, because your decision is to get us closer to this goal and customer have some point at it can support hers/his validation your decision.  -->


```
- Dear customer, looking at our goal I don't think that this will get us closer to that, so let's skip it for this moment
- Ok, you're right
```

of course this could also happen:

```
- Dear customer, looking at our goal I don't think that this will get us closer to that, so let's skip it for this moment
- No, we need that for our goal
- Please elaborate
- Reason one, reason two, reason three...
- Ok, make sense, let's plan this!
```


Also I like this analogy because it can show to non-technical person that costs of changing the goal is rising as we progress in our journey.

Imagine that you are shipping company and you need to deliver some peculiar, non-standard in shape and size cargo from Rotterdam to New York.
Ok, you are professional. You got requirements, found proper ship for this cargo, did all formalities, plan trip and so on. We are ready to go!

Now imagine changing your destination goal, for example to Buenos Aires, on different points on timeline:

- Before leave
- Just after leave
- In the middle of Atlantic
- Just on US shore

Now answer those questions:

- How cost of change looks like for every of this points?
- How rate of success for delivery will look like?

You see?

**All people in project**, on both sides (there shouldn't be two sides, but this is for another time), need to know this goal, need to understand it, need to protect it, need to be aware of costs of changing it.

Also please keep in mind, that this no silver bullet for all your problems (there are many sea monsters ahead), with delivering great software product, but I hope it will show this old problem from yet another perspective.

Cheers!