Title: Logbook entry - week 46/2022
Date: 2022-11-19
Modified: 2022-11-19
Category: logbook,
Tags: development, python, logbook, weatherbot
Slug: logbook-entry-week-46-2022
Authors: me
Summary: What happened in week 46/2022

## I'm back!

Long time has passed and many things happened. No point in going into details now. Past is past. I prefer to focus on future. So, what is the future? Small steps and just doing things, to create a habit.

## What happened in week 46?

### Did some PO/PM work on weatherbot

** Wait, what is weatherbot? **

tl;dr - bot for telegram (and/or other messaging app) that will tell you current weather.

I decided to do some super simple thing in terms of business logic and focus more on common things which makes modern saas works

- containerization
- deployment
- CI/CD pipelines
- documentation

to name a few.


### Implemented few things

Main  core application endpoint which will return current weather for given place by it's name. Not much, but it's something that implement functionality, that could be deployed. And tbh that's good for now. Everybody is thinking about new shining features, but not about how to deliver them to users in reliable and consistent experience.

#### What is reliable and consistent way?
- Make sure that prod is up, aka service availability
- Automate boring stuff (CI/CD pipelines)
- Backups and disaster recovery
- CDN, load balancing
- Security

## That's all folks!

In the next episode we will tackle containerization and actual putting this in internet.