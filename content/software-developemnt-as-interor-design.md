Title: Software developement as interior design
Date: 2020-04-25
Modified: 2019-12-10
Category: Software Craftsmanship,
Tags: development, general thoughts,
Slug: software-development-as-interior-design
Authors: me
Summary:
Status: draft


##So, what is your itch again?


There is a lot of talking about how to develop software. Even I'm not an exception from this, as you can read here.
Later I had this though about overall problem. I  think that those comparisons are good, but not suitable for every case, or speaking for otherwise, there are very suited for complex and relative big software which is build from scratch. You know, netflix, google type of things, or in terms of architecture I have in mind presentations given by titan of software world like Martin Flower or Uncle Bob. And again, those are super smart and experienced people, but I think too much people want to be like them. Build super complex and big stuff. I think that way, because I also want to do that, which probably can bee seen in my previous article.

So too not bring more confusion than is needed I would like to introduce some assumptions to my analogy.


##Why even bother with analogies?

TL;DR - it your f**** job :).

The most important thing for me would be that It gives me arguments to talk to customers, which often does not have technical background. It is my job to make things easier for them! So it is on my side to talk to them in a way they will underestand me.
This is especially important for me as a backend developer, often the only thing that I can show to the customer is JSON output or some artificial created format that shows human readable results of backend processing.

##So why building house analogy is not universal

So why not a valid analogy for me anymore? Well, we are in 2020 and almost all boring structurized parts are taken care by frameworks. You don't have to deal with transforming and handling raw http requests or tcp connections, you have orms, activeRecord,  serializations, authentication sub systems, emails sending, logging etc, etc. Almost all "low level" actions are taken care for you. So what's left?

Business logic, workflows, procssses, value proposition. Things that earns money!

So what you are getting is some standard blueprints for "raw" empty house, that you need to trasform into what you client wants and this INHO is interior designer job description. You have to work within some boundries (walls), which you can of course change to some extend (customize framework, overwrite default behaviors) but similar to real house this is very expesive to remodel walls, change windows layouts, add new floor, etc. If client wants it and resources are available (90% they are not) for that (money and time) you can do it.
